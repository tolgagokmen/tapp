google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'x');
      data.addColumn('number', 'y');

      data.addRows([
        [0, 0],   [1, 10],  [2, 23],  [3, 37],
        [18, 52], [19, 54], [20, 42], [21, 55],
        [24, 60], [25, 50], [26, 52], [27, 51],
        [36, 62], [37, 58], [38, 55], [39, 61],
        [48, 72], [49, 68], [50, 66], [51, 65],
        [60, 64], [61, 60], [62, 65], [63, 67],
        [66, 70], [67, 72], [68, 75], [69, 80]
      ]);

      var options = {
        hAxis: {
          title: 'x'
        },
        vAxis: {
          title: 'y(x)'
        },
        curveType: "function"
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
