//Toggle search pane
$( "#toggle_search" ).click( function() {
  $( "#search_form" ).toggleClass("hidden");
});

//Submit range selection
$( "#range_form" ).submit( function(event) {
  event.preventDefault();
  from_post = $( "#from" ).val();
  to_post = $( "#to" ).val();
  $.get( "part/posts/" + from_post + "-" + to_post, function( data ) {
    if (data != "") {
      $( "#posts" ).empty();
      $( "#posts" ).append( data );
      $( "#loadmore" ).addClass("hidden");
    }
  });
});

//Loading posts
var i = 0;
$.get( "part/posts/" + i, function( data ) {
  ++i;
  $( "#posts" ).append( data );
});
$( "#loadmore" ).click( function() {
  $.get( "part/posts/" + i, function( data ) {
    ++i;
    $( "#posts" ).append( data );
  });
});
