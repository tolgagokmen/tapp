#!/usr/bin/python3
import tornado.ioloop
import tornado.web
import tornado.template
import os
import datasource

class MainHandler(tornado.web.RequestHandler):
    def initialize(self, app, tab):
        self.app = app
        self.tab = tab

    def get(self):
        self.render(self.tab.template, current_tab = self.tab, tabs = self.app.tabs)

class PostsPageHandler(tornado.web.RequestHandler):
    def initialize(self, app):
        self.template = "postpack.html"
        self.app = app

    def get(self, page):
        begin = int(page) * self.app.page_limit;
        end = begin + self.app.page_limit;
        end = min(end, len(self.app.ds.posts))
        if begin <= len(self.app.ds.posts):
            self.render(self.template, first = begin, posts = self.app.ds.posts[begin:end])
        else:
            self.write("")

class PostsRangeHandler(tornado.web.RequestHandler):
    def initialize(self, app):
        self.template = "postpack.html"
        self.app = app

    def get(self, begin, end):
        begin = int(begin) - 1
        end = int(end)
        end = min(end, len(self.app.ds.posts))
        if begin < end and begin <= len(self.app.ds.posts):
            self.render(self.template, first = begin, posts = self.app.ds.posts[begin:end])
        else:
            self.write("")

class Tab:
    def __init__(self, name, route, handler=MainHandler):
        self.name = name
        self.route = route
        self.handler = handler
        self.label = name.title()
        self.template = self.name + '.html'

class Application(tornado.web.Application):
    def __init__(self):
        self.base_dir = 'res'
        self.port = 8888
        self.page_limit = 10
        self.tabs = [
            Tab("home", "/"),
            Tab("chart", "/chart"),
            Tab("posts", "/posts"),
            Tab("about", "/about")
        ]
        handlers = [(t.route, t.handler, dict(tab=t, app=self)) for t in self.tabs]
        handlers.append(("/part/posts/(\d+)", PostsPageHandler, dict(app=self)))
        handlers.append(("/part/posts/(\d+)-(\d+)", PostsRangeHandler, dict(app=self)))
        settings = {
            "debug": True,
            "template_path": os.path.join(self.base_dir, "templates"),
            "static_path": os.path.join(self.base_dir, "static")
        }
        self.ds = datasource.DataSource()
        tornado.web.Application.__init__(self, handlers, **settings)

if __name__ == "__main__":
    application = Application()
    application.listen(application.port)
    tornado.ioloop.IOLoop.instance().start()
