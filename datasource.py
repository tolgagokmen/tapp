#!/usr/bin/python3
import os

class DataSource:
    def __init__(self):
        self.loadPosts()

    def loadPosts(self):
        self.posts = []
        files = os.listdir("data/posts")
        files.sort()
        for file in files:
            self.posts.append(open("data/posts/" + file).read()[0:-1])
